#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'diagonalDifference' function below.
#
# The function is expected to return an INTEGER.
# The function accepts 2D_INTEGER_ARRAY arr as parameter.
#


'''
    arr[0]-> row 0
    arr[1]-> row 1


    arr[0][]

    for i=0 -> len(row) do
        dia-left += arr[i][i]
        dia-right += arr[i][-(i+1)]s
    end for


'''

def diagonalDifference(arr):
    diaLeft = 0
    diaRight = 0
    # Write your code here
    for i in range(len(arr[0])):
        diaLeft = diaLeft + arr[i][i]
        diaRight = diaRight + arr[i][-(i+1)]
    return abs(diaLeft-diaRight)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = []

    for _ in range(n):
        arr.append(list(map(int, input().rstrip().split())))

    result = diagonalDifference(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
