#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the twoStrings function below.
def twoStrings(s1, s2):
    word_table = {}
    for c in s1:
        v = word_table.get(c)
        if v is None:
            word_table[c] = 1

    print(word_table)
    for c in s2:
        if word_table.get(c) is not None:
            # print('YES')
            return('YES')
            
    # print('NO')
    return('NO')
    

if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # q = int(input())
    

    q =1

    for q_itr in range(q):
        s1 = 'hello'

        s2 = 'world'

        print(s1, s2)
        result = twoStrings(s1, s2)

        # fptr.write(result + '\n')

    # fptr.close()
