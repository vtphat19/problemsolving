'''
    Quick sort is an common algorithm used in practice.
    Given an array , it works as follow:
    1. A key is selected as the pivot p, for example, the first key in the array
    The pivot allows partition the array into two segments: L and G.
    The L contains keys less than the pivot p.
    On the other hand, the G contains keys larger than the pivot p.
    2. Recusively run the algorithm on segments: L and G.
    The recursion stops when there are fewer than two keys in the sequence
    3. The two segments and the pivot value are merged to produce a sorted sequence.
    We simply copy keys from segment L, the pivot value, and then keys from segment G
    
'''


# Recursive quick sort algorithm to sort an array
def quickSort(theSeq):
    n = len(theSeq)
    recQuickSort(theSeq, 0, n-1)

# The recursive implementation using virtual segments.
def recQuickSort(theSeq, first, last):
    # Check the base case
    if first >= last:
        return
    else:

        # Partition the sequence and obtain the pivot position.
        pos = partitionSeq(theSeq, first, last)
        
        # Repeat the process on the sequence and obtain the pivot position
        recQuickSort(theSeq, first, pos-1)
        
        recQuickSort(theSeq, pos+1, last)

# Paritions the subsequence using the first key as the pivot.
def partitionSeq(theSeq, first, last):
    # save copy of pivot value
    pivot = theSeq[first]

    # Find the pivot position and move the elements around the pivot
    left  = first + 1
    right = last

    while left <= right:
        # Find the left key larger than the pivot.
        while left <= right and theSeq[left] < pivot:
            left += 1

        # Find the right key smaller than the pivot.
        while right >= left and theSeq[right] >= pivot:
            right -= 1

        # Swap the two keys if we have not completed this partition.
        if left < right:
            tmp = theSeq[left]
            theSeq[left] = theSeq[right]
            theSeq[right] = tmp
        # theSeq[left], theSeq[right] = theSeq[right], theSeq[left]

    # Put the pivot in the proper position
    if right != first:
        theSeq[first] = theSeq[right]
        theSeq[right] = pivot

    # return the index position of the pivot value.
    return right


arr = [10, 9 , 8, 6, 1, 12, 23]
quickSort(arr)
print(arr)