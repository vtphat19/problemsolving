'''
    Common Known sorting algorithms are discussed
'''

def bubble_sort(array):
    '''
        The algorithm is called bubble sort, because after iteration, the smallest/largest
        element will bubble to the top of the list
        The algorithm is simple, but not efficient when
        - the array is large
        - array is in reversed ordered
        However, it is still applied when
        - array is mostly sorted, complexity is just O(n)
    '''
    n = len(array)
    # after iteration, the largest/smallest at index n-i-1 is placed in correct position
    for i in range(n-1):
        for j in range(n-i-1):
            # n-1 also brings correct result. But it takes more unncessary comparations
            if array[j] > array[j+1]:
                array[j], array[j+1] = array[j+1], array[j]

arr = [64, 34, 25, 12, 22, 11, 90]
bubble_sort(arr)
print(arr)