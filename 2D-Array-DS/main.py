#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the hourglassSum function below.
def hourglassSum(arr):
    max_s = -100
    

    for i in range(4):
        s= 0
        for j in range(4):
            s = arr[i][j] + arr[i][j+1] + arr[i][j+2] + \
                            arr[i+1][j+1] + \
                arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2]
            print('i: ', i, ' sum: ', s)
            if s > max_s:
                max_s = s
    return max_s

    

if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # arr = []

    # for _ in range(6):
    #     arr.append(list(map(int, input().rstrip().split())))

    # arr = [
    #     [1, 1, 1, 0, 0, 0],
    #     [0, 1, 0, 0, 0, 0],
    #     [1, 1, 1, 0, 0, 0],
    #     [0, 0, 2, 4, 4, 0],
    #     [0, 0, 0, 2, 0, 0],
    #     [0, 0, 1, 2, 4, 0]
    # ]

    # arr = [
    #     [-9, -9, -9, 1, 1, 1],
    #     [0,  -9, 0, 4, 3, 2],
    #     [-9, -9, -9, 1, 2, 3],
    #     [0, 0, 8, 6, 6, 0],
    #     [0, 0, 0, -2, 0, 0],
    #     [0, 0, 1, 2, 4, 0]
    # ]

    # arr = [
    #     [1, 1, 1, 0, 0, 0],
    #     [0, 1, 0, 0, 0, 0],
    #     [1, 1, 1, 0, 0, 0],
    #     [0, 0, 2, 4, 4, 0],
    #     [0, 0, 0, 2, 0, 0],
    #     [0, 0, 1, 2, 4, 0]
    # ]

    arr = [
        [-1, -1, 0, -9, -2, -2],
        [-2, -1, -6, -8, -2, -5],
        [-1, -1, -1, -2, -3, -4],
        [-1, -9, -2, -4, -4, -5],
        [-7, -3, -3, -2, -9, -9],
        [-1, -3, -1, -2, -4, -5],
    ]

    print(arr)
    result = hourglassSum(arr)
    print(result)
    # fptr.write(str(result) + '\n')

    # fptr.close()
