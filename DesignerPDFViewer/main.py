#!/bin/python3

import math
import os
import random
import re
import sys

'''
The problem is simple. To calculate the highighted area, we need to know the largest heights among the characters in the given words.
Formula:
## Area = width x height (mm^2)
it become Area(hightlighted-area) = len(word) x max(height(x_i))
x_i is character in the given word

'''

# Complete the designerPdfViewer function below.
def designerPdfViewer(h, word):    
    x = lambda c, heights: abs(int(heights[ord(c)-ord('a')]))       
    maximum = 0
    for i in word:
        if (maximum < x(i,h)):
            maximum = x(i,h) 
    return maximum*len(word)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    heights = "1 3 1 3 1 4 1 3 2 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 7"

    h = list(map(int,  heights .rstrip().split()))
    
    word = "zaba"

    result = designerPdfViewer(h, word)
    print(result)
    fptr.write(str(result) + '\n')

    fptr.close()
