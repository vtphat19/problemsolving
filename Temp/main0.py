import sys
import time

import timeit

def findMinDis(w1, w2, l):
    d = {}
    minDis = 100 # big number

    for counter, i in enumerate(l):
        if d.get(i) is None:
            d[i] = []
            d[i].append(counter)
        else:
            d[i].append(counter)

    if d.get(w1) is None or d.get(w2) is None:
        return -1


    for p1 in d[w1]:
        for p2 in d[w2]:
            distance = abs(p1-p2)
            if distance < minDis:
                minDis = distance
    
    return minDis

  
def minDist(arr, n, x, y): 
    min_dist = sys.maxsize 
  
    #Find the first occurence of any of the two numbers (x or y) 
    # and store the index of this occurence in prev 
    for i in range(n): 
          
        if arr[i] == x or arr[i] == y: 
            prev = i 
            break
   
    # Traverse after the first occurence 
    while i < n: 
        if arr[i] == x or arr[i] == y: 
  
            # If the current element matches with any of the two then 
            # check if current element and prev element are different 
            # Also check if this value is smaller than minimm distance so far 
            if arr[prev] != arr[i] and (i - prev) < min_dist : 
                min_dist = i - prev 
                prev = i 
            else: 
                prev = i 
        i += 1        
   
    return min_dist 

def minDist2(arr, n, x, y): 
    min_dist = 99999999
    for i in range(n): 
        for j in range(i + 1, n): 
            if (x == arr[i] and y == arr[j] or
            y == arr[i] and x == arr[j]) and min_dist > abs(i-j): 
                min_dist = abs(i-j) 
        return min_dist 

def main():
    l = ['cat', 'dog', 'bird', 'fish', 'cat', 'duck', 'chicken', 'dog']
    w1 = 'dog'
    w2 = 'grod'
    # l  = [3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3] 
    l = [3, 5, 4, 2, 6, 3, 0, 0, 5, 4, 8, 3, 3, 5, 4, 2, 6, 3, 0, 0, 5, 4, 8, 3, 3, 5, 4, 2, 6, 3, 0, 0, 5, 4, 8, 3, 3, 5, 4, 2, 6, 3, 0, 0, 5, 4, 8, 3] 
    w1 = 3
    w2 = 6


    
    res2 = minDist2(l, len(l), w1, w2)
    

    print(res2)

    

main()