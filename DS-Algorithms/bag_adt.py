""" Bag ADT and it's iterator """

class _BagIterator:
    """An iterator for the Bag ADT implemented as Python list."""
    def __init__(self, theList):
        self._bag_items = theList
        self._cur_item = 0
    
    def __iter__(self):
        return self
    
    def __next__(self):
        if self._cur_item < len(self._bag_items):
            item = self._bag_items[self._cur_item]
            self._cur_item += 1
            return item
        else:
            raise StopIteration

class Bag:
    """Implements the Bag ADT container using a Python list.
    """
    def __init__(self):
        self._the_items = list()

    def __len__(self):
        """Returns the number of items in bag.
        """
        return len(self._the_items)

    def __contains__(self, item):
        """Determines if an item is contained in the bag.
        """
        return item in self._the_items

    def add(self, item):
        """Adds a new item to the bag.
        """
        self._the_items.append(item)

    def remove(self, item):
        """Removes and returns an instance of the item from the bag.
        """
        assert item in self._the_items, "The item must be in the bag."
        ndx = self._the_items.index(item)
        return self._the_items.pop(ndx)

    def __iter__(self):
        return _BagIterator(self._the_items)