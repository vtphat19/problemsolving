#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the maximumToys function below.
def maximumToys(prices, k):
    '''
        Sort array in ascending order.
        Then apply greedy algorithm.
        Greedy works because we do have any objective except selecting 
        as much items as possible. 
        The approach is starting from smallest price item, try to 
        select it if there is enough money.
    '''
    prices.sort()

    count = 0
    for i,v in enumerate(prices):
        if k - v >= 0:
            k = k - v
            count = count + 1
    
    return count
        


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    prices = list(map(int, input().rstrip().split()))

    result = maximumToys(prices, k)

    fptr.write(str(result) + '\n')

    fptr.close()
