#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the countSwaps function below.
def countSwaps(a):
    totalSwaps = 0
    n = len(a)
    for i in range(n-1):
        for j in range(n-i-1):
            if a[j] > a[j+1]:
                totalSwaps = totalSwaps + 1
                a[j], a[j+1] = a[j+1], a[j]

    print('Array is sorted in %s swaps.' %(totalSwaps))
    print('First Element: %s' %(a[0]))
    print('Last Element: %s' %(a[n-1]))

if __name__ == '__main__':
    n = int(input())

    a = list(map(int, input().rstrip().split()))

    countSwaps(a)
