#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the minimumBribes function below.
def minimumBribes(q):
    '''
    Follow lsming solution: 
    https://www.hackerrank.com/challenges/new-year-chaos/forum/comments/143969
    '''
    brides = 0 
    for i in range(len(q)-1, -1, -1): # loop from tail of list -> first entry
        if q[i] -  (i +1)  > 2:
            print('Too chaotic')
            return
        for j in range(max(0, q[i]-2), i):
            if q[j] > q[i]:
                brides = brides + 1
    print(brides)
    return



if __name__ == '__main__':
    t = int(input())

    for t_itr in range(t):
        n = int(input())

        q = list(map(int, input().rstrip().split()))

        minimumBribes(q)
