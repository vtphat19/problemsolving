#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the makeAnagram function below.
def makeAnagram(a, b):
    
    if len(a) < len(b):
        a, b = b, a

    table = {}

    for char in a:
        try:
            table[char] += 1
        except:
            table[char] = 1
    
    newChar = {}

    for char in b:
        try:
            if table[char] > 0:
                table[char] -= 1
            else:
                if newChar.get(char) == None:
                    newChar[char] = 1
                else:
                    newChar[char] += 1                
        except:
            if newChar.get(char) == None:
                newChar[char] = 1
            else:
                newChar[char] += 1

    total = 0 

    for k, v in table.items():
        total += v
    for k, v in newChar.items():
        total += v

        
    return total

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    a = input()

    b = input()

    res = makeAnagram(a, b)

    fptr.write(str(res) + '\n')

    fptr.close()
