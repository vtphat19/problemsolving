#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the checkMagazine function below.
def checkMagazine(magazine, note):
    # print(magazine)
    # print(note)
    words = {}
    for w in magazine:
        v = words.get(w, None)
        if v != None:
            words[w] = words[w] +1
        else:
            words[w] = 1
                        
    print(words)

    for w in note:
        v = words.get(w, None)
        print('v: ', w)
        if v is None:
            print(w)
            print('No')
            return
        else:
            words[w] = words[w] - 1
            if words[w] == 0:
                words.pop(w)
    print('Yes')


if __name__ == '__main__':
    # mn = input().split()

    # m = int(mn[0])

    # n = int(mn[1])

    # magazine = input().rstrip().split()

    # note = input().rstrip().split()

    m = 6
    n = 5

    magazine = ['two', 'times', 'three', 'is', 'not', 'four']
    note = ['two', 'times', 'two', 'is', 'four']

    checkMagazine(magazine, note)
