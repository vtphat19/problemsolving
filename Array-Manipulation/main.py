#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the arrayManipulation function below.
def arrayManipulation(n, queries):
    '''
        O(n*k) solution. This is not acceptable due to timeout constraint.
    '''
    # arr = [0 for i in range(n)]
    # maxNum = -1
    # # print(queries)
    # for i in range(len(queries)):
    #     op = queries[i]
    #     for j in range(op[0]-1, op[1]):
    #         arr[j] = arr[j] + op[2]
    #         if arr[j] > maxNum:
    #             maxNum = arr[j]

    '''
        O(n+k) solution. Apply prefix sum
    '''
    arr = [0 for i in range(n+2)]
    maxNum = 0
    # print(queries)
    for i in range(len(queries)):
        op = queries[i]
        a = op[0]
        b = op[1]
        k = op[2]

        arr[a] =arr[a] + k
        arr[b+1] =arr[b+1] - k

    temSum = 0
    for i in range(1, n+1):
        # arr[i] = arr[i-1] + arr[i] # this actually slower than the line below
        temSum = temSum + arr[i]
        if maxNum < temSum:
            maxNum = temSum
    

    return maxNum

# Complete the arrayManipulation function below.
def arrayManipulation(n, queries):
    # print(n)
    arr = [0 for i in range(n+2)]
    maxNum = 0
    # print(queries)
    for i in range(len(queries)):
        op = queries[i]
        a = op[0]
        b = op[1]
        k = op[2]

        arr[a] =arr[a] + k
        arr[b+1] =arr[b+1] - k

    temSum = 0
    for i in range(1, n+1):
        # arr[i] = arr[i-1] + arr[i] # this actually slower than the line below
        temSum = temSum + arr[i]
        if maxNum < temSum:
            maxNum = temSum
    
    return maxNum

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nm = input().split()

    n = int(nm[0])

    m = int(nm[1])

    queries = []

    for _ in range(m):
        queries.append(list(map(int, input().rstrip().split())))

    result = arrayManipulation(n, queries)

    fptr.write(str(result) + '\n')

    fptr.close()
